﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MouseLogger {
	public delegate void MouseClickDelegate(MouseButtons button, int x, int y);
	public delegate void MouseMoveDelegate(int x, int y);
	public delegate void KeyboardPressDelegate(uint keyCode);

	class MouseHook {
		public static MouseMoveDelegate mouseMove;
		public static MouseClickDelegate mouseClick;
		public static KeyboardPressDelegate keyPress;

		public static void start() {
			callback = new LowLevelMouseProc(HookCallback);
			callbackKb = new KeyboardProc(HookCallbackKeyboard);

			_hookID = SetWindowsHookEx(WH_MOUSE_LL, callback, GetModuleHandle("user32"), 0);
			_keyHookID = SetWindowsHookEx(WH_KEYBOARD_LL, callbackKb, GetModuleHandle("user32"), 0);
		}

		public static void stop() {
			UnhookWindowsHookEx(_hookID);
			UnhookWindowsHookEx(_keyHookID);
		}

		private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam) {
			if (nCode >= 0) {
				MSLLHOOKSTRUCT hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));

				switch ((MouseMessages)wParam) {
				case MouseMessages.WM_LBUTTONDOWN:
					//var clickArgs = new MouseEventArgs(MouseButtons.Left, 0, hookStruct.pt.x, hookStruct.pt.y, 0);
					mouseClick(MouseButtons.Left, hookStruct.pt.x, hookStruct.pt.y);
					break;
				case MouseMessages.WM_RBUTTONDOWN:
					//var clickArgsR = new MouseEventArgs(MouseButtons.Right, 0, hookStruct.pt.x, hookStruct.pt.y, 0);
					mouseClick(MouseButtons.Right, hookStruct.pt.x, hookStruct.pt.y);
					break;
				case MouseMessages.WM_MOUSEMOVE:
					//var moveArgs = new MouseEventArgs(MouseButtons.None, 0, hookStruct.pt.x, hookStruct.pt.y, 0);
					mouseMove(hookStruct.pt.x, hookStruct.pt.y);
					break;
				}
			}

			return CallNextHookEx(_hookID, nCode, wParam, lParam);
		}

		private static IntPtr HookCallbackKeyboard(int nCode, IntPtr wParam, IntPtr lParam) {
			const Int64 mask = (1 << 7);
			const int WM_KEYDOWN = 0x0100;

			if (nCode == 0 && (Int64)wParam == WM_KEYDOWN) {
				KBDLLHOOKSTRUCT hookStruct = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(KBDLLHOOKSTRUCT));

				bool initialPress = (hookStruct.flags & mask) == 0;
				if (initialPress) {
					keyPress(hookStruct.vkCode);
				}
			}
			
			return CallNextHookEx(_keyHookID, nCode, wParam, lParam);
		}

		private delegate IntPtr LowLevelMouseProc(int nCode, IntPtr wParam, IntPtr lParam);
		private delegate IntPtr KeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
		private static LowLevelMouseProc callback;
		private static KeyboardProc callbackKb;
		private static IntPtr _hookID = IntPtr.Zero;
		private static IntPtr _keyHookID = IntPtr.Zero;

		/*
		 * Unmanaged function imports
		 */

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelMouseProc lpfn, IntPtr hMod, uint dwThreadId);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr SetWindowsHookEx(int idHook, KeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool UnhookWindowsHookEx(IntPtr hhk);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr GetModuleHandle(string lpModuleName);

		/*
		 * Structures used by unmanaged code
		 */

		private enum MouseMessages {
			WM_LBUTTONDOWN = 0x0201,
			WM_LBUTTONUP = 0x0202,
			WM_MOUSEMOVE = 0x0200,
			WM_MOUSEWHEEL = 0x020A,
			WM_RBUTTONDOWN = 0x0204,
			WM_RBUTTONUP = 0x0205
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct POINT {
			public int x;
			public int y;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct MSLLHOOKSTRUCT {
			public POINT pt;
			public uint mouseData;
			public uint flags;
			public uint time;
			public IntPtr dwExtraInfo;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct KBDLLHOOKSTRUCT {
			public uint vkCode;
			public uint scanCode;
			public uint flags;
			public uint time;
			public IntPtr dwExtraInfo;
		}

		private const int WH_KEYBOARD_LL = 13;
		private const int WH_MOUSE_LL = 14;
	}
}
