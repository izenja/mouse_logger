﻿namespace MouseLogger
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.positionLabel = new System.Windows.Forms.Label();
			this.startButton = new System.Windows.Forms.Button();
			this.stopButton = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.filePathTextBox = new System.Windows.Forms.TextBox();
			this.statusLabel = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label4 = new System.Windows.Forms.Label();
			this.centerXbox = new System.Windows.Forms.TextBox();
			this.centerYbox = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(82, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Mouse Position:";
			// 
			// positionLabel
			// 
			this.positionLabel.AutoSize = true;
			this.positionLabel.Location = new System.Drawing.Point(100, 9);
			this.positionLabel.Name = "positionLabel";
			this.positionLabel.Size = new System.Drawing.Size(13, 13);
			this.positionLabel.TabIndex = 2;
			this.positionLabel.Text = "0";
			// 
			// startButton
			// 
			this.startButton.Location = new System.Drawing.Point(151, 86);
			this.startButton.Name = "startButton";
			this.startButton.Size = new System.Drawing.Size(120, 25);
			this.startButton.TabIndex = 4;
			this.startButton.Text = "Start Log";
			this.startButton.UseVisualStyleBackColor = true;
			this.startButton.Click += new System.EventHandler(this.startButton_Click);
			// 
			// stopButton
			// 
			this.stopButton.Location = new System.Drawing.Point(151, 117);
			this.stopButton.Name = "stopButton";
			this.stopButton.Size = new System.Drawing.Size(120, 25);
			this.stopButton.TabIndex = 5;
			this.stopButton.Text = "Stop Log";
			this.stopButton.UseVisualStyleBackColor = true;
			this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 27);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(84, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Log file location:";
			// 
			// filePathTextBox
			// 
			this.filePathTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.filePathTextBox.Location = new System.Drawing.Point(102, 25);
			this.filePathTextBox.Name = "filePathTextBox";
			this.filePathTextBox.ReadOnly = true;
			this.filePathTextBox.Size = new System.Drawing.Size(170, 20);
			this.filePathTextBox.TabIndex = 7;
			// 
			// statusLabel
			// 
			this.statusLabel.AutoSize = true;
			this.statusLabel.Location = new System.Drawing.Point(148, 145);
			this.statusLabel.Name = "statusLabel";
			this.statusLabel.Size = new System.Drawing.Size(98, 13);
			this.statusLabel.TabIndex = 8;
			this.statusLabel.Text = "Status: not logging.";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 52);
			this.label2.TabIndex = 9;
			this.label2.Text = "Start (2D): F5\r\nStart (3D FPS): F6\r\nMark: F8\r\nStop: F9";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Location = new System.Drawing.Point(12, 86);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(133, 72);
			this.groupBox1.TabIndex = 10;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Controls";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 54);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(66, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Center (x, y):";
			// 
			// centerXbox
			// 
			this.centerXbox.Location = new System.Drawing.Point(102, 51);
			this.centerXbox.Name = "centerXbox";
			this.centerXbox.Size = new System.Drawing.Size(50, 20);
			this.centerXbox.TabIndex = 12;
			this.centerXbox.Text = "959";
			// 
			// centerYbox
			// 
			this.centerYbox.Location = new System.Drawing.Point(158, 51);
			this.centerYbox.Name = "centerYbox";
			this.centerYbox.Size = new System.Drawing.Size(50, 20);
			this.centerYbox.TabIndex = 13;
			this.centerYbox.Text = "539";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 166);
			this.Controls.Add(this.centerYbox);
			this.Controls.Add(this.centerXbox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.statusLabel);
			this.Controls.Add(this.filePathTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.stopButton);
			this.Controls.Add(this.startButton);
			this.Controls.Add(this.positionLabel);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Mouse Logger";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label positionLabel;
		private System.Windows.Forms.Button startButton;
		private System.Windows.Forms.Button stopButton;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox filePathTextBox;
		private System.Windows.Forms.Label statusLabel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox centerXbox;
		private System.Windows.Forms.TextBox centerYbox;
    }
}

