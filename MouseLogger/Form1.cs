﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MouseLogger {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();

			MouseHook.start();
			MouseHook.mouseClick = new MouseClickDelegate(mouseClicked);
			MouseHook.mouseMove = new MouseMoveDelegate(mouseMoved);
			MouseHook.keyPress = new KeyboardPressDelegate(keyPressed);

			this.filePathTextBox.Text = Path.GetFullPath(logFileName);
		}

		private void keyPressed(uint key) {
			Keys k = (Keys)key;
			switch(k) {
			case Keys.F5:
				startLog(false);
				break;
			case Keys.F6:
				startLog(true);
				break;
			case Keys.F8:
				mark();
				break;
			case Keys.F9:
				stopLog();
				break;
			}
		}

		void startLog(bool fps) {
			if (logging)
				return;

			this.fps = fps;
			centerX = Int32.Parse(centerXbox.Text);
			centerY = Int32.Parse(centerYbox.Text);
			virtX = 0;
			virtY = 0;

			stopWatch.Start();
			logging = true;
			statusLabel.Text = fps ? "Status: logging (FPS)." : "Status: logging.";
		}

		void stopLog() {
			stopWatch.Stop();
			this.fps = false;
			logging = false;
			statusLabel.Text = "Status: not logging.";

			logWriter.Flush();
		}

		void mark() {
			if (!logging)
				return;

			logWriter.Flush();
			logWriter.Write(stopWatch.ElapsedMilliseconds);
			logWriter.WriteLine(",mark,0,0");
			logWriter.Flush();
		}

		private void mouseClicked(MouseButtons button, int x, int y) {
			string pos = x.ToString() + "," + y.ToString();
			this.positionLabel.Text = pos;

			if (logging) {
				string buttonId = "O";
				if (button == MouseButtons.Left)
					buttonId = "L";
				else if (button == MouseButtons.Right)
					buttonId = "R";

				var elapsed = stopWatch.ElapsedMilliseconds;
				logWriter.Write(elapsed);
				logWriter.Write(",c" + buttonId + ",");
				logWriter.WriteLine(pos);
			}
		}

		private void mouseMoved(int x, int y) {
			curX = x;
			curY = y;
			virtX += x - centerX;
			virtY += y - centerY;

			string pos;
			if (fps) {
				pos = virtX.ToString() + "," + virtY.ToString();
			} else {
				pos = x.ToString() + "," + y.ToString();
			}
			this.positionLabel.Text = pos;

			if (logging) {
				var elapsed = stopWatch.ElapsedMilliseconds;
				logWriter.Write(elapsed);
				logWriter.Write(",m,");
				logWriter.WriteLine(pos);
			}
		}

		private void startButton_Click(object sender, EventArgs e) {
			startLog(false);
		}

		private void stopButton_Click(object sender, EventArgs e) {
			stopLog();
		}

		const string logFileName = "mouse_log.txt";
		StreamWriter logWriter = new StreamWriter(logFileName, true);
		bool logging = false;

		bool fps = false;
		int curX;
		int curY;
		int virtX;
		int virtY;
		int centerX;
		int centerY;

		Stopwatch stopWatch = new Stopwatch();
	}
}
